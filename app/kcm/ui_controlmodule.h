#include <kdialog.h>
#include <klocale.h>

/********************************************************************************
** Form generated from reading UI file 'controlmodule.ui'
**
** Created: Sun Oct 30 20:34:46 2011
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTROLMODULE_H
#define UI_CONTROLMODULE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QFormLayout>
#include <QtGui/QFrame>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "alignmentselector.h"
#include "kcombobox.h"

QT_BEGIN_NAMESPACE

class Ui_ControlModule
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_3;
    QFrame *stateContainer;
    QHBoxLayout *horizontalLayout_2;
    QLabel *stateIconLabel;
    QLabel *stateTextLabel;
    QPushButton *startButton;
    QCheckBox *kcfg_AutoStart;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_5;
    QFormLayout *formLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    Colibri::AlignmentSelector *alignmentSelector;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_3;
    KComboBox *screenComboBox;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *previewButton;
    QSpacerItem *horizontalSpacer_4;
    QLabel *previewImpossibleLabel;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *ControlModule)
    {
        if (ControlModule->objectName().isEmpty())
            ControlModule->setObjectName(QString::fromUtf8("ControlModule"));
        ControlModule->resize(417, 361);
        verticalLayout = new QVBoxLayout(ControlModule);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(ControlModule);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_3 = new QVBoxLayout(groupBox);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        stateContainer = new QFrame(groupBox);
        stateContainer->setObjectName(QString::fromUtf8("stateContainer"));
        stateContainer->setAutoFillBackground(true);
        stateContainer->setFrameShape(QFrame::Box);
        stateContainer->setFrameShadow(QFrame::Plain);
        horizontalLayout_2 = new QHBoxLayout(stateContainer);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        stateIconLabel = new QLabel(stateContainer);
        stateIconLabel->setObjectName(QString::fromUtf8("stateIconLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(stateIconLabel->sizePolicy().hasHeightForWidth());
        stateIconLabel->setSizePolicy(sizePolicy);
        stateIconLabel->setMinimumSize(QSize(22, 22));

        horizontalLayout_2->addWidget(stateIconLabel);

        stateTextLabel = new QLabel(stateContainer);
        stateTextLabel->setObjectName(QString::fromUtf8("stateTextLabel"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(stateTextLabel->sizePolicy().hasHeightForWidth());
        stateTextLabel->setSizePolicy(sizePolicy1);
        stateTextLabel->setText(QString::fromUtf8("[current notification system information]"));
        stateTextLabel->setWordWrap(true);

        horizontalLayout_2->addWidget(stateTextLabel);

        startButton = new QPushButton(stateContainer);
        startButton->setObjectName(QString::fromUtf8("startButton"));
        sizePolicy.setHeightForWidth(startButton->sizePolicy().hasHeightForWidth());
        startButton->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(startButton);


        verticalLayout_3->addWidget(stateContainer);

        kcfg_AutoStart = new QCheckBox(groupBox);
        kcfg_AutoStart->setObjectName(QString::fromUtf8("kcfg_AutoStart"));

        verticalLayout_3->addWidget(kcfg_AutoStart);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(ControlModule);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        horizontalLayout_5 = new QHBoxLayout(groupBox_2);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        alignmentSelector = new Colibri::AlignmentSelector(groupBox_2);
        alignmentSelector->setObjectName(QString::fromUtf8("alignmentSelector"));

        horizontalLayout->addWidget(alignmentSelector);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        screenComboBox = new KComboBox(groupBox_2);
        screenComboBox->setObjectName(QString::fromUtf8("screenComboBox"));

        horizontalLayout_3->addWidget(screenComboBox);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        previewButton = new QPushButton(groupBox_2);
        previewButton->setObjectName(QString::fromUtf8("previewButton"));

        horizontalLayout_4->addWidget(previewButton);

        horizontalSpacer_4 = new QSpacerItem(158, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);


        formLayout->setLayout(2, QFormLayout::FieldRole, horizontalLayout_4);

        previewImpossibleLabel = new QLabel(groupBox_2);
        previewImpossibleLabel->setObjectName(QString::fromUtf8("previewImpossibleLabel"));
        sizePolicy1.setHeightForWidth(previewImpossibleLabel->sizePolicy().hasHeightForWidth());
        previewImpossibleLabel->setSizePolicy(sizePolicy1);
        previewImpossibleLabel->setWordWrap(true);

        formLayout->setWidget(3, QFormLayout::FieldRole, previewImpossibleLabel);


        horizontalLayout_5->addLayout(formLayout);

        horizontalSpacer = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer);


        verticalLayout->addWidget(groupBox_2);

        verticalSpacer_2 = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);


        retranslateUi(ControlModule);

        QMetaObject::connectSlotsByName(ControlModule);
    } // setupUi

    void retranslateUi(QWidget *ControlModule)
    {
        groupBox->setTitle(tr2i18n("Status", 0));
        stateIconLabel->setText(QString());
        startButton->setText(tr2i18n("Start Colibri", 0));
        kcfg_AutoStart->setText(tr2i18n("Start Colibri at login", 0));
        groupBox_2->setTitle(tr2i18n("Appearance", 0));
        label->setText(tr2i18n("Notification position:", 0));
        label_2->setText(tr2i18n("Notification screen:", 0));
        previewButton->setText(tr2i18n("Preview", 0));
        previewImpossibleLabel->setText(tr2i18n("Cannot preview because Colibri is not running.", 0));
        Q_UNUSED(ControlModule);
    } // retranslateUi

};

namespace Ui {
    class ControlModule: public Ui_ControlModule {};
} // namespace Ui

QT_END_NAMESPACE

#endif // CONTROLMODULE_H

