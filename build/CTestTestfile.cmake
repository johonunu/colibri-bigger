# CMake generated Testfile for 
# Source directory: /home/nikola/colibri-colibri
# Build directory: /home/nikola/colibri-colibri/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(app)
SUBDIRS(kcm)
SUBDIRS(po)
