# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Caio Romão Costa Nascimento <contact@caioromao.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2010-11-13 20:51-0200\n"
"PO-Revision-Date: 2010-08-06 11:56+0200\n"
"Last-Translator: Caio Romão Costa Nascimento <contact@caioromao.com>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: app/about.h:26
msgid "Light notification system for KDE4"
msgstr "Sistema leve de notificação para o KDE4"

#: app/about.h:32
msgid "Colibri"
msgstr "Colibrí"

#: app/about.h:35
msgid "(C) 2009-2010 Aurélien Gâteau"
msgstr "(C) 2009-2010 Aurélien Gâteau"

#: app/about.h:37
msgid "Aurélien Gâteau"
msgstr "Aurélien Gâteau"

#: kcm/controlmodule.cpp:158
msgid "Colibri is running."
msgstr "Colibrí está em execução."

#: kcm/controlmodule.cpp:163
msgid "No notification system is running."
msgstr "Nenhum sistema de notificação rodando."

#: kcm/controlmodule.cpp:166
msgid ""
"The current notification system is %1. You must stop it to be able to start "
"Colibri."
msgstr ""
"O atual sistema de notificação é %1. É necessário pará-lo para initiar o "
"Colibrí."

#. i18n: file: kcm/controlmodule.ui:176
#. i18n: ectx: property (text), widget (QPushButton, previewButton)
#: kcm/controlmodule.cpp:221 po/rc.cpp:23 rc.cpp:23
msgid "Preview"
msgstr "Visualização"

#: kcm/controlmodule.cpp:222
msgid "This is a preview of a Colibri notification"
msgstr "Esta é uma prévia de uma notificação do Colibri"

#: kcm/controlmodule.cpp:236
msgid "Screen under mouse"
msgstr "Tela sob o mouse"

#: kcm/controlmodule.cpp:240
msgid "Screen %1"
msgstr "Tela %1"

#. i18n: file: kcm/controlmodule.ui:17
#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: po/rc.cpp:4 rc.cpp:4
msgid "Status"
msgstr "Estado"

#. i18n: file: kcm/controlmodule.ui:76
#. i18n: ectx: property (text), widget (QPushButton, startButton)
#: po/rc.cpp:8 rc.cpp:8
msgid "Start Colibri"
msgstr "Iniciar Colibrí"

#. i18n: file: kcm/controlmodule.ui:86
#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AutoStart)
#: po/rc.cpp:11 rc.cpp:11
msgid "Start Colibri at login"
msgstr "Iniciar o Colibri ao logar-se"

#. i18n: file: kcm/controlmodule.ui:96
#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: po/rc.cpp:14 rc.cpp:14
msgid "Appearance"
msgstr "Aparência"

#. i18n: file: kcm/controlmodule.ui:117
#. i18n: ectx: property (text), widget (QLabel, label)
#: po/rc.cpp:17 rc.cpp:17
msgid "Notification position:"
msgstr "Posição da notificação:"

#. i18n: file: kcm/controlmodule.ui:147
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: po/rc.cpp:20 rc.cpp:20
msgid "Notification screen:"
msgstr "Tela da notificação:"

#. i18n: file: kcm/controlmodule.ui:204
#. i18n: ectx: property (text), widget (QLabel, previewImpossibleLabel)
#: po/rc.cpp:26 rc.cpp:26
msgid "Cannot preview because Colibri is not running."
msgstr "Não é possível visualizar pois Calibri não está em execução."

#: po/rc.cpp:27 rc.cpp:27
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Caio Romão Costa Nascimento"

#: po/rc.cpp:28 rc.cpp:28
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "contact@caioromao.com"
